# Gittex
Un pequeñísimo script para actualizar de forma automática un repositorio GIT

## Requisitos
Instalar libnotify-bin

`sudo apt install libnotify-bin`

Cambiar el nombre del archivo `_.gitignore` a `.gitignore` para evitar que los archivos de Gittex sean subidos a tu repositorio
